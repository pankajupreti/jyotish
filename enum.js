const yogList = {
    1: {
        title: "Ruchaka Yoga",
        planets: "Ma",
        inHouse: [1, 4, 7, 10],
        disposition: ["UNCHA", "MOOLTRIKON", "SWAGRIHA"]
    },
    2: {
        title: "Bhadra Yoga",
        planets: "Me",
        inHouse: [1, 4, 7, 10],
        disposition: ["UNCHA", "MOOLTRIKON", "SWAGRIHA"]
    },
    3: {
        title: "Hamsa Yoga",
        planets: "Ju",
        inHouse: [1, 4, 7, 10],
        disposition: ["UNCHA", "MOOLTRIKON", "SWAGRIHA"]
    },
    4: {
        title: "Malavya Yoga",
        planets: "Ve",
        inHouse: [1, 4, 7, 10],
    },
    5: {
        title: "Shasha Yoga",
        planets: "Sa",
        inHouse: [1, 4, 7, 10],
        disposition: ["UNCHA", "MOOLTRIKON", "SWAGRIHA"]
    },
    6: {
        title: "Gaj Kesari Yoga",
        planets: ["Mo", "Ju"],
        aspect: "quad",
        enhanceif: {
            disposition: ["UNCHA", "MOOLTRIKON", "SWAGRIHA"]
        },
        excludeif: {
            disposition: "retrograde"
        }
    },
    7: {
        title: "Lakmi Yoga",
        planets: ["L9", "L1"],
        disposition: ["UNCHA", "MOOLTRIKON", "SWAGRIHA"],
        aspect: ["together", "opposite"]
    },
    8: {
        title: "Harsha Vipreet Raj Yoga",
        planets: ["L6"],
        inHouse: [8, 12]
    },
    9: {
        title: "Sarala Vipreet Raj Yoga",
        planets: ["L8"],
        inHouse: [6, 8, 12]
    },
    10: {
        title: "Vimala Vipreet Raj Yoga",
        planets: ["L12"],
        inHouse: [6, 8, 12]
    },
    11: {
        title: "Neecha Bhanga Raj Yoga",
        planetsByDisposition: ["NEECHA", ["UNCHA", "MOOLTRIKON", "SWAGRIHA"]],
        aspect: ["together", "opposite"]
    },
    12: {
        title: "Kalanidhi Yoga",
        planets: ["Ju", "Me"],
        disposition: ["UNCHA", "MOOLTRIKON", "SWAGRIHA"],
        inHouse: [2]
    },
    13: {
        title: "Kalanidhi Yoga",
        planets: ["Ju", "Ve"],
        disposition: ["UNCHA", "MOOLTRIKON", "SWAGRIHA"],
        inHouse: [2]
    }
}

export {yogList}